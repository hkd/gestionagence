/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import entities.Agence;
import entities.Client;
import entities.Compte;
import entities.Operation;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author koodateur
 */
public interface DataInterface extends Remote 
{

    public boolean ajouterAgence(Agence a)  throws RemoteException;

    public boolean modifierAgence(Agence a)  throws RemoteException;

    public boolean supprimerAgence(Agence a)  throws RemoteException;

    public ArrayList<Agence> listeAgences()  throws RemoteException;

    public int nombreClient(Agence agence)  throws RemoteException;

    public boolean ajouterClient(Client c)  throws RemoteException;

    public boolean modifierClient(Client c)  throws RemoteException;

    public boolean supprimerClient(Client c) throws RemoteException;

    public ArrayList<Client> listeClients() throws RemoteException;

    public ArrayList<Client> listeClients(Agence agence)  throws RemoteException;

    public Client findClient(int id) throws RemoteException;

    public boolean ajouterCompte(Compte c) throws RemoteException;

    public boolean modifierCompte(Compte c) throws RemoteException;

    public boolean supprimerCompte(Compte c)  throws RemoteException;

    public ArrayList<Compte> listeComptes()  throws RemoteException;

    public ArrayList<Compte> listeComptes(Client client)  throws RemoteException;

    public boolean ajouterOperation(Operation o) throws RemoteException;

    public boolean modifierOperation(Operation o)  throws RemoteException;

    public boolean supprimerOperation(Operation o) throws RemoteException;

    public ArrayList<Operation> listeOperations() throws RemoteException;

    public ArrayList<Operation> listeOperations(Compte c) throws RemoteException;

    public ArrayList<Operation> listeOperations(Client cl) throws RemoteException;

}