/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import dao.AccesBD;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import presentation.Ecran;

/**
 *
 * @author Hamadou DAO
 */
public class Serveur {

    public Serveur() {
        AccesBD s;
        try {
            s = new AccesBD();
            LocateRegistry.createRegistry(1099);
            Naming.rebind("rmi://localhost/BK", s);
        } catch (RemoteException | MalformedURLException ex) {
            System.err.println(ex.getMessage());
        }
    }
}
