/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;

/**
 *
 * @author Hamadou DAO
 */
public class Compte implements Serializable  {
    private String numcpt;
    private String libcpt;
    private String sens;
    private String solde;
    private Client client;

    public Compte() {
    }

    public Compte(String numcpt, String libcpt, String sens, String solde, Client client) {
        this.numcpt = numcpt;
        this.libcpt = libcpt;
        this.sens = sens;
        this.solde = solde;
        this.client = client;
    }

    public Client getClient() {
        return client;
    }

    public String getLibcpt() {
        return libcpt;
    }

    public String getNumcpt() {
        return numcpt;
    }

    public String getSens() {
        return sens;
    }

    public String getSolde() {
        return solde;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public void setLibcpt(String libcpt) {
        this.libcpt = libcpt;
    }

    public void setNumcpt(String numcpt) {
        this.numcpt = numcpt;
    }

    public void setSens(String sens) {
        this.sens = sens;
    }

    public void setSolde(String solde) {
        this.solde = solde;
    }

    @Override
    public String toString() {
        return numcpt+" "+libcpt; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
