/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import entities.Agence;
import entities.Client;
import entities.Compte;
import entities.Operation;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import metier.DataInterface;

/**
 *
 * @author Hamadou DAO
 */
/**
 * @since Cette classe permet au serveur de faire des requêtes sur la base de
 * données
 */
public class AccesBD  extends UnicastRemoteObject implements DataInterface {

    /**
     * Variable de connexion a la base de donnée et de stockage des données
     * 
     */
    private static final String USERNAME = "root";
    private static final String PASSWORD = "";
    private static final String ADDRESS = "localhost/banque";
    private Connection con = null;
    private PreparedStatement st = null;
    private ResultSet rs = null;

    /**
     * Constructeur de la classe
     */
     public AccesBD() throws RemoteException {
    }

     /**
     * @since permet de se connecter a la base de données
     * @return 
     */
    public boolean connec() {
        fermer();
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://" + ADDRESS, USERNAME, PASSWORD);
        } catch (ClassNotFoundException | SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            return false;
        }
        return true;
    }
/**
     * @since permet de fermet la base de données après chaque operation
     * @return 
     */
    public boolean fermer() {
        try {
            con.close();
        } catch (SQLException | NullPointerException ex) {
            System.out.println("!!!!" + ex.getMessage());
            return false;
        }
        return true;
    }
    /**
     * Gestion des agences
     * @Fonction : ajouter,modifier,supprimer,lister
     */
    /**
     * @since permet d'ajouter une agence
     * @param agence
     * @return 
     */
    public boolean ajouterAgence(Agence a) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "INSERT INTO Agence(nomag,adresseag) values (?,?)";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, a.getNomag());
            st.setString(2, a.getAdresseag());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public boolean modifierAgence(Agence a) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "UPDATE Agence SET nomag=?,adresseag=? WHERE numag=?";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, a.getNomag());
            st.setString(2, a.getAdresseag());
            st.setInt(3, a.getNumag());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public boolean supprimerAgence(Agence a) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "DELETE FROM Agence WHERE numag=?";
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, a.getNumag());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public ArrayList<Agence> listeAgences() {
        ArrayList<Agence> liste = new ArrayList<>();
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return liste;
        }
        String sql = "SELECT numag,nomag,adresseag FROM Agence ORDER BY nomag";
        try {
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Agence a = new Agence();
                a.setNumag(rs.getInt("numag"));
                a.setNomag(rs.getString("nomag"));
                a.setAdresseag(rs.getString("adresseag"));
                liste.add(a);
            }

        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return liste;
        }
        fermer();
        return liste;
    }

    public int nombreClient(Agence agence) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return 0;
        }
        String sql = "SELECT count(numag) as nombre FROM Client WHERE numag=?";
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, agence.getNumag());
            rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("nombre");
            }

        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return -1;
        }
        fermer();
        return -1;
    }

    /**
     * Gestion client
     * @Fonction : ajouter,modifier,supprimer,lister
     * @Fonction : lister client d'une agence
     */
    public boolean ajouterClient(Client c) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "INSERT INTO Client(nomcli,prenomcli,numag) values (?,?,?)";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, c.getNomcli());
            st.setString(2, c.getPrenomcli());
            st.setInt(3, c.getAgence().getNumag());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public boolean modifierClient(Client c) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "UPDATE Client SET nomcli=?,prenomcli=?,numag=? WHERE numcli=?";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, c.getNomcli());
            st.setString(2, c.getPrenomcli());
            st.setInt(3, c.getAgence().getNumag());
            st.setInt(4, c.getNumcli());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public boolean supprimerClient(Client c) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "DELETE FROM Client WHERE numcli=?";
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, c.getNumcli());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public ArrayList<Client> listeClients() {
        ArrayList<Client> liste = new ArrayList<>();
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return liste;
        }
        String sql = "SELECT c.numcli,c.nomcli,c.prenomcli,a.numag,a.nomag,a.adresseag"
                + " FROM Client c,Agence a WHERE c.numag=a.numag ORDER BY c.nomcli,c.prenomcli";
        try {
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Client c = new Client();
                c.setNumcli(rs.getInt("numcli"));
                c.setNomcli(rs.getString("nomcli"));
                c.setPrenomcli(rs.getString("prenomcli"));
                Agence a = new Agence();
                a.setNumag(rs.getInt("numag"));
                a.setNomag(rs.getString("nomag"));
                a.setAdresseag(rs.getString("adresseag"));
                c.setAgence(a);
                liste.add(c);
            }
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
        }
        fermer();
        return liste;
    }

    public ArrayList<Client> listeClients(Agence agence) {
        ArrayList<Client> liste = new ArrayList<>();
        if (!connec()) {
            return liste;
        }
        String sql = "SELECT * FROM Client WHERE numag=? ORDER BY nomcli,prenomcli";
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, agence.getNumag());
            rs = st.executeQuery();
            while (rs.next()) {
                Client c = new Client();
                c.setNumcli(rs.getInt("numcli"));
                c.setNomcli(rs.getString("nomcli"));
                c.setPrenomcli(rs.getString("prenomcli"));
                Agence a = agence;
                c.setAgence(a);
                liste.add(c);
            }
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return null;
        }
        fermer();
        return liste;
    }

    public Client findClient(int id) {
        if (!connec()) {
            return null;
        }
        String sql = "SELECT * FROM Client WHERE numcli=?";
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, id);
            rs = st.executeQuery();
            Client c = new Client();
            c.setNumcli(rs.getInt("numcli"));
            c.setNomcli(rs.getString("nomcli"));
            c.setPrenomcli(rs.getString("prenomcli"));
            fermer();
            return c;
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
        }
        fermer();
        return null;
    }

    /**
     * Gestion Compte
     * @Fonction : ajouter,modifier,supprimer,lister
     * @Fonction :lister comptes d'un client
     */
    public boolean ajouterCompte(Compte c) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "INSERT INTO Compte(numcpt,libcpt,numcli) VALUES (?,?,?)";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, c.getNumcpt());
            st.setString(2, c.getLibcpt());
            st.setInt(3, c.getClient().getNumcli());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        sql = "INSERT INTO operation(libop, montop, sensop, numcpt) VALUES (?,?,?,?)";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, "crediteur");
            st.setString(2, c.getSolde());
            st.setString(3, "CR");
            st.setString(4, c.getNumcpt());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage()+"impossible d'effectuer l'opération");
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public boolean modifierCompte(Compte c) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "UPDATE Compte SET libcpt=?,numcli=? WHERE numcpt=?";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, c.getLibcpt());
            st.setInt(2, c.getClient().getNumcli());
            st.setString(3, c.getNumcpt());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public boolean supprimerCompte(Compte c) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "DELETE FROM Compte WHERE numcpt=?";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, c.getNumcpt());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public ArrayList<Compte> listeComptes() {
        ArrayList<Compte> liste = new ArrayList<>();
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return liste;
        }
        String sql = "SELECT cc.*,c.*,"+
                "ifnull((SELECT SUM(montop) FROM Operation Where sensop='CR' AND numcpt=cc.numcpt),0) as cr,"+
                "ifnull((SELECT SUM(montop) FROM Operation Where sensop='DB' AND numcpt=cc.numcpt),0) as db "+
                "FROM Compte cc,Client c where cc.numcli=c.numcli ORDER BY cc.libcpt";
        try {
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Compte c = new Compte();
                c.setLibcpt(rs.getString("libcpt"));
                c.setNumcpt(rs.getString("numcpt"));
                double cr=rs.getDouble("cr");
                double db=rs.getDouble("db");
                c.setSens((cr>db)?"CR":"DB");
                c.setSolde(""+(cr-db));
                Client cc = new Client();
                cc.setNumcli(rs.getInt("numcli"));
                cc.setNomcli(rs.getString("nomcli"));
                cc.setPrenomcli(rs.getString("prenomcli"));
                c.setClient(cc);
                liste.add(c);
            }
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return null;
        }
        fermer();
        return liste;
    }

    public ArrayList<Compte> listeComptes(Client client) {
        ArrayList<Compte> liste = new ArrayList<>();
        if (!connec()) {
            return liste;
        }
        String sql = "SELECT *,"+
                "(SELECT SUM(montop) FROM Operation Where sensop='CR' AND numcpt=cc.numcpt) as cr,"+
                "(SELECT SUM(montop) FROM Operation Where sensop='DB' AND numcpt=cc.numcpt) as db "+
                "FROM Compte cc WHERE numcli=? ORDER BY libcpt";
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, client.getNumcli());
            rs = st.executeQuery();
            while (rs.next()) {
                Compte c = new Compte();
                c.setClient(client);
                c.setLibcpt(rs.getString("libcpt"));
                c.setNumcpt(rs.getString("numcpt"));
                double cr=rs.getDouble("cr");
                double db=rs.getDouble("db");
                c.setSens((cr>db)?"CR":"DB");
                c.setSolde(""+(cr-db));
                liste.add(c);
            }
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
        }
        fermer();
        return liste;
    }

    /**
     * Gestion operation
     * @Fonction : ajouter,supprimer, lister
     * @Fonction : lister operations d'un compte,lister operations des comptes d'un client
     */
    public boolean ajouterOperation(Operation o) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "INSERT INTO operation(libop, montop, sensop, numcpt) VALUES (?,?,?,?)";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, o.getLibop());
            st.setString(2, o.getMontop());
            st.setString(3, o.getSensop());
            st.setString(4, o.getCompte().getNumcpt());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public boolean modifierOperation(Operation o) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "UPDATE Operation SET libop=?, montop=?, sensop=?, numcpt=? WHERE numop=?";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, o.getLibop());
            st.setString(2, o.getMontop());
            st.setString(3, o.getSensop());
            st.setString(4, o.getCompte().getNumcpt());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public boolean supprimerOperation(Operation o) {
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return false;
        }
        String sql = "DELETE FROM Operation WHERE numop=?";
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, o.getNumop());
            st.execute();
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return false;
        }
        fermer();
        return true;
    }

    public ArrayList<Operation> listeOperations() {
        ArrayList<Operation> liste = new ArrayList<>();
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return liste;
        }
        String sql = "SELECT co.*,o.*,c.* FROM Operation o,compte co,Client c where "
                + "o.numcpt=co.numcpt AND co.numcli=c.numcli order by o.dateop";
        try {
            st = con.prepareStatement(sql);
            rs = st.executeQuery();
            while (rs.next()) {
                Operation o = new Operation();
                o.setDateop(SimpleDateFormat.getDateInstance().format(rs.getDate("dateop")));
                o.setLibop(rs.getString("libop"));
                o.setMontop(rs.getString("montop"));
                o.setNumop(rs.getInt("numop"));
                o.setSensop(rs.getString("sensop"));
                Compte c = new Compte();
                c.setLibcpt(rs.getString("libcpt"));
                c.setNumcpt(rs.getString("numcpt"));
                Client cc = new Client();
                cc.setNumcli(rs.getInt("numcli"));
                cc.setNomcli(rs.getString("nomcli"));
                cc.setPrenomcli(rs.getString("prenomcli"));
                c.setClient(cc);
                o.setCompte(c);
                liste.add(o);
            }
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return null;
        }
        fermer();
        return liste;
    }

    public ArrayList<Operation> listeOperations(Compte c) {
        ArrayList<Operation> liste = new ArrayList<>();
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return null;
        }
        String sql = "SELECT * FROM Operation o where "
                + "numcpt=? order by o.dateop";
        try {
            st = con.prepareStatement(sql);
            st.setString(1, c.getNumcpt());
            rs = st.executeQuery();
            while (rs.next()) {
                Operation o = new Operation();
                o.setDateop(SimpleDateFormat.getDateInstance().format(rs.getDate("dateop")));
                o.setLibop(rs.getString("libop"));
                o.setMontop(rs.getString("montop"));
                o.setNumop(rs.getInt("numop"));
                o.setSensop(rs.getString("sensop"));
                o.setCompte(c);
                liste.add(o);
            }
        } catch (SQLException ex) {
            System.out.println("!!!" + ex.getMessage());
            fermer();
            return null;
        }
        fermer();
        return liste;
    }

    public ArrayList<Operation> listeOperations(Client cl) {
        ArrayList<Operation> liste = new ArrayList<>();
        if (!connec()) {
            System.err.println("!!!! Impossible de se connecter");
            return liste;
        }
        String sql = "SELECT co.*,o.*,c.* FROM Operation o,compte co,Client"
                + " c where o.numcpt=co.numcpt and co.numcli=c.numcli and c.numcli=? order by o.dateop";
        try {
            st = con.prepareStatement(sql);
            st.setInt(1, cl.getNumcli());
            rs = st.executeQuery();
            while (rs.next()) {
                Operation o = new Operation();
                o.setDateop(SimpleDateFormat.getDateInstance().format(rs.getDate("dateop")));
                o.setLibop(rs.getString("libop"));
                o.setMontop(rs.getString("montop"));
                o.setNumop(rs.getInt("numop"));
                o.setSensop(rs.getString("sensop"));
                Compte c = new Compte();
                c.setLibcpt(rs.getString("libcpt"));
                c.setNumcpt(rs.getString("numcpt"));
                o.setCompte(c);
                liste.add(o);
            }
        } catch (SQLException ex) {
            System.out.println("!!!!" + ex.getMessage());
            fermer();
            return null;
        }
        fermer();
        return liste;
    }

}
