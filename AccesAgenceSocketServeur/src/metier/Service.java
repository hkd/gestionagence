/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import dao.AccesBD;
import entities.Agence;
import entities.Client;
import entities.Compte;
import entities.Operation;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.awt.image.WritableRaster;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import presentation.Ecran;

/**
 *
 * @author Hamadou DAO
 */
public class Service extends Thread {

    private final Socket socket;
    private final AccesBD bd;
    ObjectInputStream ois;
    ObjectOutputStream oos;
    DataOutputStream out;
    DataInputStream in;
    boolean reponse_etat;

    public Service(Socket socket) {
        bd = new AccesBD();
        this.socket = socket;
    }

    public void run() {
        try {
            ois = new ObjectInputStream(socket.getInputStream());
            oos = new ObjectOutputStream(socket.getOutputStream());
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
            while (traitement());
        } catch (IOException | ClassNotFoundException ex) {
            System.out.println("****" + ex.getMessage());
            Ecran.deconnecter();
            Ecran.message.append("client deconnecté " + socket.getInetAddress() + "\n");
        }
    }

    public boolean traitement() throws IOException, ClassNotFoundException {
        String mode = (String) ois.readObject();
        Ecran.message.append(mode + " " + socket.getInetAddress().getHostAddress() + "\n");
        switch (mode) {
            case "ajouter agence":
                reponse_etat = bd.ajouterAgence((Agence) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "modifier agence":
                reponse_etat = bd.modifierAgence((Agence) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "supprimer agence":
                reponse_etat = bd.supprimerAgence((Agence) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "lister agence":
                oos.writeObject(bd.listeAgences());
                oos.flush();
                break;

            case "ajouter client":
                reponse_etat = bd.ajouterClient((Client) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "modifier client":
                reponse_etat = bd.modifierClient((Client) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "supprimer client":
                reponse_etat = bd.supprimerClient((Client) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "nombre de client par agence":
                out.writeInt(bd.nombreClient((Agence) ois.readObject()));
                out.flush();
                break;
            case "la liste des clients":
                oos.writeObject(bd.listeClients());
                oos.flush();
                break;
            case "la liste des clients d'une agence":
                oos.writeObject(bd.listeClients((Agence) ois.readObject()));
                oos.flush();
                break;
            case "ajouter compte":
                reponse_etat = bd.ajouterCompte((Compte) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "modifier compte":
                reponse_etat = bd.modifierCompte((Compte) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "supprimer compte":
                reponse_etat = bd.supprimerCompte((Compte) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "la liste des comptes":
                oos.writeObject(bd.listeComptes());
                oos.flush();
                break;
            case "la liste des comptes d'un client":
                oos.writeObject(bd.listeComptes((Client) ois.readObject()));
                oos.flush();
                break;
            case "ajouter operation":
                reponse_etat = bd.ajouterOperation((Operation) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "modifier operation":
                reponse_etat = bd.modifierOperation((Operation) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "supprimer operation":
                reponse_etat = bd.supprimerOperation((Operation) ois.readObject());
                out.writeBoolean(reponse_etat);
                out.flush();
                break;
            case "la liste des operations":
                oos.writeObject(bd.listeOperations());
                oos.flush();
                break;
            case "la liste des operations d'un client":
                oos.writeObject(bd.listeOperations((Client) ois.readObject()));
                oos.flush();
                break;
            case "la liste des operations d'un compte client":
                oos.writeObject(bd.listeOperations((Compte) ois.readObject()));
                oos.flush();
                break;
            case "ma photo de profil":

                break;
            case "fin de la connexion":
                oos.flush();
                Ecran.deconnecter();
                Ecran.message.append("deconnecter Client Connecter " + socket.getInetAddress().getHostAddress() + "\n");
                return false;
        }
        return true;
    }
}
