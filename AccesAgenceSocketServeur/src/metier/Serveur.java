/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import presentation.Ecran;

/**
 * Cette classe permet au serveur d'attendre un client et de l'associer a un thread en cas de connexion
 */
/**
 * 
 * @author Hamadou DAO
 */
public class Serveur {

    public Serveur() {
        try {
            ServerSocket serv = new ServerSocket(8000);
            Ecran.message.append("Le serveur a bien demarré \n");
            while (true) {
                Socket socket = serv.accept();
                Service s = new Service(socket);
                s.start();
                Ecran.connecter();
                Ecran.message.append("Nouveau Client Connecter " + socket.getInetAddress().getHostAddress()+"\n");
            }

        } catch (IOException ex) {
            Ecran.message.append("Impossible de demarer le serveur "+ex.getMessage()+"\n");
        }
    }
}
