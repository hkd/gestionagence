/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;

/**
 *
 * @author Hamadou DAO
 */
public class Operation implements Serializable  {
    private int numop;
    private String libop;
    private String dateop;
    private String montop;
    private String sensop;
    private Compte compte;

    public Operation() {
    }

    public String getDateop() {
        return dateop;
    }

    public String getLibop() {
        return libop;
    }

    public String getMontop() {
        return montop;
    }

    public Compte getCompte() {
        return compte;
    }

    public int getNumop() {
        return numop;
    }

    public String getSensop() {
        return sensop;
    }

    public void setDateop(String dateop) {
        this.dateop = dateop;
    }

    public void setLibop(String libop) {
        this.libop = libop;
    }

    public void setMontop(String montop) {
        this.montop = montop;
    }

    public void setCompte(Compte compte) {
        this.compte = compte;
    }


    public void setNumop(int numop) {
        this.numop = numop;
    }

    public void setSensop(String sensop) {
        this.sensop = sensop;
    }
    
    
    
}
