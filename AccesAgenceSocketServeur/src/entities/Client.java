/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;

/**
 *
 * @author Hamadou DAO
 */
public class Client implements Serializable  {
    private int numcli;
    private String nomcli;
    private String prenomcli;
    /**
     * @see un client peut s'inscrire dans une et une seule agence
     */
    private Agence agence;

    public Client() {
    }
    
    public Client(int numcli, String nomcli, String prenomcli, Agence agence) {
        this.numcli = numcli;
        this.nomcli = nomcli;
        this.prenomcli = prenomcli;
        this.agence = agence;
    }

    public int getNumcli() {
        return numcli;
    }

    public void setNumcli(int numcli) {
        this.numcli = numcli;
    }

    public String getNomcli() {
        return nomcli;
    }

    public void setNomcli(String nomcli) {
        this.nomcli = nomcli;
    }

    public String getPrenomcli() {
        return prenomcli;
    }

    public void setPrenomcli(String prenomcli) {
        this.prenomcli = prenomcli;
    }

    public Agence getAgence() {
        return agence;
    }

    public void setAgence(Agence agence) {
        this.agence = agence;
    }

    @Override
    public String toString() {
        return nomcli+" "+prenomcli; //To change body of generated methods, choose Tools | Templates.
    }
    
}
