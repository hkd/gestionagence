/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entities;

import java.io.Serializable;
import presentation.Start;

/**
 * Classe Agence avec getters et setters
 */
/**
 *
 * @author Hamadou DAO
 */
public class Agence implements Serializable {

    private int numag;
    private String nomag;
    private String adresseag;

    public Agence() {
    }

    public Agence(int numag, String nomag, String adresseag) {
        this.numag = numag;
        this.nomag = nomag;
        this.adresseag = adresseag;
    }

    public int getNumag() {
        return numag;
    }

    public void setNumag(int numag) {
        this.numag = numag;
    }

    public String getNomag() {
        return nomag;
    }

    public void setNomag(String nomag) {
        this.nomag = nomag;
    }

    public String getAdresseag() {
        return adresseag;
    }

    public void setAdresseag(String adresseag) {
        this.adresseag = adresseag;
    }
     public int getClientCount() {
      return Start.accesBD.nombreClient(this);
    }

    @Override
    public String toString() {
        return nomag; //To change body of generated methods, choose Tools | Templates.
    }
     
}
