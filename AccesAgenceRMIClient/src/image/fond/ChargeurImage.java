/**
 *
 * @author Hamadou DAO
 */
package image.fond;

import java.net.URL;
import javax.swing.ImageIcon;
import metier.ErrFichier;

/**
 * @param Cette classe permet de gérer les images, si les images n'existe pas il
 * retournera une image vide tout en generant une erreur
 */
public class ChargeurImage extends ImageIcon {

    public ChargeurImage(String chemin) {
        super();
        if (getClass().getResource(chemin) == null) {
            try {
                throw new ErrFichier("Le fichier n'existe pas");
            } catch (ErrFichier ex) {
                System.out.println(ex.getMessage());
            }
        } else {
            setImage((new ImageIcon(getClass().getResource(chemin))).getImage());
        }
    }
}
