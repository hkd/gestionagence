/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import entities.Agence;
import entities.Client;
import entities.Compte;
import entities.Operation;
import image.fond.ChargeurImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import presentation.VariableGlobale;
import presentation.msg_Information;

/**
 *  <p> cette classe sert d'une interface entre la presentation des vues et objet recuperer a distance
 * @author Hamadou DAO
 */
public class Service {

    /**
    * Object distant recuperer dans la variable objetDistant
    */
    private DataInterface objetDistant;
    
    /*
    * L'adresse est enregistrer dans une variable et on pourait la modifier facilement
    */
    private String adresse;
    /*
    * Constructeur de la classe publique service
    */
    public Service() {
    }
    
    /* methode permetant de recuperer une instance de l'objet distant*/
    public DataInterface getObjetDistant() {
        return objetDistant;
    }

    /*
    * Methode permetant de se connecter pour recuperer l'object distant
    * La methode lit un fichier contenant une adresse. Plus besoin de fixer l'adresse statique
    */
    @SuppressWarnings("empty-statement")
    public void seConnecter() {
        File f = new File("data/adresse");
        adresse = "localhost";

        try (BufferedReader lire = new BufferedReader(new FileReader(f))) {
            adresse = lire.readLine();
            lire.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            objetDistant = (DataInterface) Naming.lookup("rmi://" + adresse + ":1099/BK");
        } catch (NotBoundException | MalformedURLException | RemoteException ex) {
            new msg_Information("Impossible de se connecter");
        }
    }

    /* 
    Methode permetant de verifier si la connexion a reussi
    */
    public boolean verif() {
        if (!isConnecter()) {
            new msg_Information("Veuillez-vous connecter");
            return true;
        }
        return false;
    }

    /*
    permet de recuperer l'adresse
    */
    public String getAdresse() {
        return adresse;
    }
    /*
    verifie si l'objet distant est disponible
    */
    public boolean isConnecter() {
        if (objetDistant != null) {
            return true;
        } else {
            return false;
        }
    }

    /*
    permet de recuperer une photo ou image
    */
    public ImageIcon recupererPhoto() {
        if (!isConnecter()) {
            return new ChargeurImage(VariableGlobale.cheminPhoto);
        }
        return new ImageIcon(VariableGlobale.imageClient);
    }
    
    
    /*
    * Nous avons ici toutes les methodes d'agence
    */
    /*
    permet d'ajouter une agence
    */
    public boolean ajouterAgence(Agence a) {
        boolean resultat = false;
        try {
            resultat = getObjetDistant().ajouterAgence(a);
        } catch (RemoteException ex) {
        }
        return resultat;
    }
    /* 
    permet de modifier une agence
    */
    public boolean modifierAgence(Agence a){
    boolean resultat = false;
        try {
            resultat = getObjetDistant().modifierAgence(a);
        } catch (RemoteException ex) {
        }
        return resultat;
    }
    /*
    permet de supprimer une agence
    */
    public boolean supprimerAgence(Agence a){
    boolean resultat = false;
        try {
            resultat = getObjetDistant().supprimerAgence(a);
        } catch (RemoteException ex) {
        }
        return resultat;
    }
    /* 
    permet de lister les agences
    */
    public ArrayList<Agence> listeAgences(){
    ArrayList<Agence>listeAgence=new ArrayList<>();
        try {
            listeAgence=getObjetDistant().listeAgences();
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return listeAgence;
    }
    /*
    permet d'afficher le nombre de client par agence
    */
    public int nombreClient(Agence agence){
    int nombre=0;
        try {
            nombre=getObjetDistant().nombreClient(agence);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return nombre;
    }
    /*
    *  <p> Partie gestion de client
    *   <p> nous avons les fonctions qui permet d'ajouter, de modifier et de supprimer
    *   <p> en plus nous avons une fonction pour afficer les clients d'une agence
    */
    public boolean ajouterClient(Client c){
    boolean resultat=false;
        try {
            resultat=getObjetDistant().ajouterClient(c);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return resultat;
    }

    public boolean modifierClient(Client c){
    boolean resultat=false;
        try {
            resultat=getObjetDistant().modifierClient(c);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return resultat;
    }

    public boolean supprimerClient(Client c){
    boolean resultat=false;
        try {
            resultat=getObjetDistant().supprimerClient(c);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return resultat;
    }

    public ArrayList<Client> listeClients(){
    ArrayList<Client>listeClients=new ArrayList<>();
        try {
            listeClients=getObjetDistant().listeClients();
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return listeClients;
    }

    public ArrayList<Client> listeClients(Agence agence){
    ArrayList<Client>listeClients=new ArrayList<>();
        try {
            listeClients=getObjetDistant().listeClients(agence);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return listeClients;
    }

    /**
     * 
     * @param id
     * @return 
     */
    public Client findClient(int id){
    Client client=null;
        try {
            client=getObjetDistant().findClient(id);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return client;
    }
    
    /*
    * Gestion des comptes
    * @Fonction: ajouter, modifier, supprimer,
    * @Fonction :lister les comptes d'un client
    */
    /**
     * 
     * @param compte
     * @return 
     */
    public boolean ajouterCompte(Compte c){
    boolean resultat=false;
        try {
            resultat=getObjetDistant().ajouterCompte(c);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return resultat;
    }
    public boolean modifierCompte(Compte c){
    boolean resultat=false;
        try {
            resultat=getObjetDistant().modifierCompte(c);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return resultat;
    }

    public boolean supprimerCompte(Compte c){
    boolean resultat=false;
        try {
            resultat=getObjetDistant().supprimerCompte(c);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return resultat;
    }

    public ArrayList<Compte> listeComptes(){
    ArrayList<Compte>listeComptes=new ArrayList<>();
        try {
            listeComptes=getObjetDistant().listeComptes();
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return listeComptes;
    }

    public ArrayList<Compte> listeComptes(Client client){
    ArrayList<Compte>listeComptes=new ArrayList<>();
        try {
            listeComptes=getObjetDistant().listeComptes(client);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return listeComptes;
    }
    
    /**
     * Gestion des operations
     * @Fonction : ajouter, modifier,supprimer,lister operation, lister operation d'un compte
     * @Fonction : lister operation de tous les comptes d'un client
     */
    /**
     * 
     * @param operation
     * @return 
     */
    public boolean ajouterOperation(Operation o){
        boolean resultat = false;
        try {
            resultat = getObjetDistant().ajouterOperation(o);
        } catch (RemoteException ex) {
        }
        return resultat;
    }

    public boolean modifierOperation(Operation o){
        boolean resultat = false;
        try {
            resultat = getObjetDistant().modifierOperation(o);
        } catch (RemoteException ex) {
        }
        return resultat;
    }

    public boolean supprimerOperation(Operation o){
        boolean resultat = false;
        try {
            resultat = getObjetDistant().supprimerOperation(o);
        } catch (RemoteException ex) {
        }
        return resultat;
    }

    public ArrayList<Operation> listeOperations(){
    ArrayList<Operation>listeOperations=new ArrayList<>();
        try {
            listeOperations=getObjetDistant().listeOperations();
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return listeOperations;
    }

    public ArrayList<Operation> listeOperations(Compte c){
    ArrayList<Operation>listeOperations=new ArrayList<>();
        try {
            listeOperations=getObjetDistant().listeOperations(c);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return listeOperations;
    }

    public ArrayList<Operation> listeOperations(Client cl){
    ArrayList<Operation>listeOperations=new ArrayList<>();
        try {
            listeOperations=getObjetDistant().listeOperations(cl);
        } catch (RemoteException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
    return listeOperations;
    }
}
