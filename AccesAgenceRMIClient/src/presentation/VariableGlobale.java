/**
 *
 * @author Hamadou DAO
 */
package presentation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @param Cette classe permet de gérer les variables globales en cequi conserne
 * les informations qui ne changent pas rapidement
 */
public class VariableGlobale {

    public static String cheminFondEcran = "/image/fond/6.png";
    final static String cheminIcon = "/image/icon.png";
    final static String cheminEye = "/image/eye.png";
    final static String cheminPrec = "/image/prec.png";
    final static String cheminSuiv = "/image/suiv.png";
    final static String cheminAgence = "/image/agence.png";
    final static String cheminSetting = "/image/setting.png";
    final static String cheminOk = "/image/ok.png";
    final static String cheminRetour = "/image/retour.png";
    public final static String cheminPhoto = "/image/photo.png";
    public final static String imageClient = "data/images/photo";
    final static String nomLogiciel = "Banque SGBS";
    final static String cheminFaireOperation = "/image/faireoperation.png";
    final static String cheminBanque = "/image/banque.jpg";
    final static String cheminEuro = "/image/euro.png";
    final static String cheminGestionAgence = "/image/gestionagence.png";
    final static String cheminAjouter = "/image/add.png";
    final static String cheminModifier = "/image/modify.png";
    final static String cheminSupprimer = "/image/supprimer.png";
    final static String cheminBar = "/image/bar.png";
    final static String cheminReleveCompte = "/image/relevecompte.png";
    final static String cheminAjouterClient = "/image/ajouterclient.png";
    final static String cheminGestionClient = "/image/gestionclient.png";
    final static String cheminGestionOperation = "/image/gestionoperation.png";
    final static String cheminPresentation = "/image/presentation.png";
    final static String cheminGestionCompte = "/image/gestioncompte.png";
    final static String cheminOperation = "/image/operation.png";
    final static String cheminVert = "/image/vert.png";
    final static String cheminRouge = "/image/rouge.png";
    final static String cheminNoir = "/image/noir.png";
    final static String cheminSacharge = "/image/attente.gif";
    final static String cheminTransparent = "/image/transparent.png";

    /**
     *
     * @param source
     * @param dest
     * @since cette methode permet de copier un fichier d'un repertoire a un
     * autre
     * @return boolean
     */
    public static boolean copier(File source, File dest) {
        try (InputStream sourceFile = new java.io.FileInputStream(source);
                OutputStream destinationFile = new FileOutputStream(dest)) {
            // Lecture par segment de 0.5Mo  
            byte buffer[] = new byte[512 * 1024];
            int nbLecture;
            while ((nbLecture = sourceFile.read(buffer)) != -1) {
                destinationFile.write(buffer, 0, nbLecture);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return false; // Erreur 
        }
        return true;
    }

    public static void saveFondEcran() {
        try {
            PrintWriter ecrire = new PrintWriter(new File("data/saveFondEcran"));
            ecrire.println(cheminFondEcran);
            ecrire.close();
        } catch (FileNotFoundException ex) {
        }
    }

    public static void initFondEcran() {
        try {
            BufferedReader lire = new BufferedReader(new FileReader("data/saveFondEcran"));
            try {
                String chemin = lire.readLine();
                if (chemin != null) {
                    cheminFondEcran = chemin;
                   Start.i=Integer.parseInt(chemin.substring(chemin.lastIndexOf("/")+1,chemin.lastIndexOf(".")));System.out.println("j");
                }
                lire.close();
            } catch (IOException ex) {
                Logger.getLogger(VariableGlobale.class.getName()).log(Level.SEVERE, null, ex);
            }

        } catch (FileNotFoundException ex) {
        }
    }
}
