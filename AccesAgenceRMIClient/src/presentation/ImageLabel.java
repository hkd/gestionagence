
package presentation;

import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JPanel;

/**
 *@since cette classe permet de mettre une image dans un panneau et de le
 * modifier à volonter l'idée est que l'image prend la taille totale du panneau
 * 
 * @author Hamadou DAO
 */
public class ImageLabel extends JPanel {

    private String chemin;
    private ImageIcon image;
    private Image im;
    public ImageLabel() {
    }

    public ImageLabel(String chemin) {
        this.chemin = chemin;
    }

    public void setChemin(String chemin) {
        this.chemin = chemin;
        this.image=null;
        repaint();
    }
    public void setImageIcon(ImageIcon image) {
        this.image = image;
        this.chemin=null;
        repaint();
    }
    public void setImage(Image image) {
        this.im = image;
        this.chemin=null;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        if(image==null)
        g.drawImage(new ImageIcon(chemin).getImage(), 0, 0, this.getWidth(), this.getHeight(), this);
        else
        g.drawImage(image.getImage(), 0, 0, this.getWidth(), this.getHeight(), this);
    }

    public void initDefaultChemin() {
        chemin = VariableGlobale.imageClient;
        repaint();
    }

}
