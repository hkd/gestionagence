-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 03 août 2018 à 18:04
-- Version du serveur :  5.7.21
-- Version de PHP :  7.0.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `banque`
--

-- --------------------------------------------------------

--
-- Structure de la table `agence`
--

DROP TABLE IF EXISTS `agence`;
CREATE TABLE IF NOT EXISTS `agence` (
  `numag` int(11) NOT NULL AUTO_INCREMENT,
  `nomag` varchar(30) NOT NULL,
  `adresseag` varchar(255) NOT NULL,
  PRIMARY KEY (`numag`),
  UNIQUE KEY `nom` (`nomag`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `agence`
--

INSERT INTO `agence` (`numag`, `nomag`, `adresseag`) VALUES
(3, 'Burkina Faso', 'Gueule Tapée rue 64x59'),
(7, 'Kolobanne', 'Rue pricesse, cote d\'ivoire foire'),
(8, 'Asta Funding, Inc.', 'Texas'),
(9, 'Keysight Technologies Inc.', 'Pennsylvania'),
(10, 'Hingham Institution for Saving', 'New Jersey'),
(11, 'Lindblad Expeditions Holdings ', 'Alaska'),
(12, 'Rocky Brands, Inc.', 'Texas'),
(13, 'Great Plains Energy Inc', 'California'),
(14, 'Terex Corporation', 'California'),
(15, 'The Cushing MLP Total Return F', 'Texas'),
(16, 'Intrexon Corporation', 'Virginia'),
(17, 'First Trust Nasdaq Retail ETF', 'California'),
(18, 'Gray Television, Inc.', 'Oklahoma'),
(19, 'Senomyx, Inc.', 'Nevada'),
(20, 'Akebia Therapeutics, Inc.', 'Delaware'),
(21, 'Liberty TripAdvisor Holdings, ', 'California'),
(22, 'Prudential Financial, Inc.', 'Arizona'),
(23, 'Central Garden & Pet Company', 'California'),
(24, 'Zions Bancorporation', 'Missouri'),
(25, 'AllianzGI Equity & Convertible', 'California'),
(26, 'West Corporation', 'Nebraska'),
(27, 'Principal Price Setters Index ', 'Pennsylvania');

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

DROP TABLE IF EXISTS `client`;
CREATE TABLE IF NOT EXISTS `client` (
  `numcli` int(11) NOT NULL AUTO_INCREMENT,
  `nomcli` varchar(20) NOT NULL,
  `prenomcli` varchar(20) NOT NULL,
  `numag` int(11) NOT NULL,
  PRIMARY KEY (`numcli`),
  KEY `numag` (`numag`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `client`
--

INSERT INTO `client` (`numcli`, `nomcli`, `prenomcli`, `numag`) VALUES
(3, 'Sidibé', 'Dangaloum', 3),
(4, 'Sawadogo', 'Aziz', 7),
(6, 'Ronaldo', 'Cristiano', 7),
(7, 'Robertsson', 'Thor', 3),
(8, 'Stellman', 'Cary', 3),
(9, 'Pedley', 'Marcel', 3),
(10, 'Davley', 'Clemmy', 3),
(11, 'Orred', 'Adelind', 7),
(12, 'Stothart', 'Catha', 7),
(16, 'Smy', 'Jojo', 7),
(17, 'Birkhead', 'Giuseppe', 7),
(18, 'Labrow', 'Simonne', 7),
(19, 'Elford', 'Marina', 7),
(20, 'Franck', 'Alberto', 8),
(21, 'Detloff', 'Hyacinthe', 8),
(22, 'Wyness', 'Carlo', 8),
(23, 'Mapplethorpe', 'Meridel', 8),
(24, 'Marchenko', 'Zerk', 8),
(25, 'Gounel', 'Karrie', 8),
(26, 'Kynsey', 'Kendall', 9),
(27, 'Brightling', 'Vilma', 9),
(28, 'Moreno', 'Lonna', 9),
(29, 'Peterffy', 'Sterling', 9),
(30, 'Fasler', 'Nelly', 9),
(31, 'Purseglove', 'Mariya', 9),
(32, 'DAO', 'Hamadou', 3),
(33, 'DAO', 'Alimata', 20),
(34, 'DAO', 'Zakari', 20),
(35, 'Konaté', 'Alimata', 24);

-- --------------------------------------------------------

--
-- Structure de la table `compte`
--

DROP TABLE IF EXISTS `compte`;
CREATE TABLE IF NOT EXISTS `compte` (
  `numcpt` varchar(10) NOT NULL,
  `libcpt` varchar(20) NOT NULL,
  `numcli` int(11) NOT NULL,
  PRIMARY KEY (`numcpt`),
  KEY `numcli` (`numcli`) COMMENT='cle etrangere'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `compte`
--

INSERT INTO `compte` (`numcpt`, `libcpt`, `numcli`) VALUES
('0000012', 'Bonjour 2019', 34),
('002312329', '#Faro Faro', 35),
('02068102', 'Compte familial', 3),
('02114991', 'Compte personnel', 32),
('029321342', 'FootBall', 6),
('03114991', 'Compte affaire', 32),
('0439321232', 'Compte bien commun', 35),
('049348321', 'Compte de ma mère', 35);

-- --------------------------------------------------------

--
-- Structure de la table `operation`
--

DROP TABLE IF EXISTS `operation`;
CREATE TABLE IF NOT EXISTS `operation` (
  `numop` int(11) NOT NULL AUTO_INCREMENT,
  `libop` varchar(20) NOT NULL,
  `dateop` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `montop` double(12,2) NOT NULL,
  `sensop` enum('DB','CR','','') NOT NULL,
  `numcpt` varchar(10) NOT NULL,
  PRIMARY KEY (`numop`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `operation`
--

INSERT INTO `operation` (`numop`, `libop`, `dateop`, `montop`, `sensop`, `numcpt`) VALUES
(1, 'Opération de débit', '2018-05-13 18:02:29', 1000.00, 'DB', 'BC2018'),
(2, 'Opération de débit', '2018-05-15 09:43:44', 3000.00, 'DB', 'BC2018'),
(3, 'Opération de crédit', '2018-05-15 09:43:59', 400000.00, 'CR', 'CBAO 2018'),
(4, 'Opération de crédit', '2018-05-15 09:44:23', 4000.00, 'CR', 'BC2018'),
(7, 'Opération de crédit', '2018-06-05 10:03:07', 50000.00, 'CR', 'V.I.P 2019'),
(8, 'Opération de crédit', '2018-06-20 08:56:15', 300000.00, 'CR', '03114991'),
(9, 'Opération de débit', '2018-06-20 08:56:29', 50000.00, 'DB', '03114991'),
(10, 'Opération de crédit', '2018-06-20 08:56:44', 600000.00, 'CR', '03114991'),
(11, 'Opération de crédit', '2018-06-20 08:57:00', 25000.00, 'CR', '02068102'),
(12, 'crediteur', '2018-06-20 09:01:49', 4000000.00, 'CR', '029321342'),
(13, 'Opération de débit', '2018-06-20 09:22:44', 3000000.00, 'DB', '02114991'),
(14, 'Opération de débit', '2018-06-25 15:39:52', 100000.00, 'DB', '03114991'),
(15, 'Opération de crédit', '2018-06-25 15:45:38', 3000000.00, 'CR', '02068102'),
(16, 'Opération de crédit', '2018-06-25 15:49:51', 4000000.00, 'CR', '03114991'),
(17, 'Opération de débit', '2018-06-25 15:57:19', 5000000.00, 'CR', '02114991'),
(18, 'crediteur', '2018-06-25 16:46:28', 305000.00, 'CR', '0000012'),
(19, 'Opération de débit', '2018-06-25 17:12:09', 150000.00, 'DB', '03114991'),
(20, 'Opération de débit', '2018-07-08 15:48:20', 500000.00, 'DB', '02114991'),
(21, 'Opération de débit', '2018-07-08 16:04:49', 150000.00, 'DB', '02114991'),
(22, 'Opération de débit', '2018-07-08 19:33:36', 4000000.00, 'DB', '02068102'),
(23, 'crediteur', '2018-07-16 13:32:53', 10000.00, 'CR', '002312329'),
(24, 'crediteur', '2018-07-16 13:33:34', 300000.00, 'CR', '0439321232'),
(25, 'crediteur', '2018-07-16 13:34:03', 500000.00, 'CR', '049348321'),
(26, 'Opération de crédit', '2018-07-16 13:35:52', 1000000.00, 'CR', '02068102'),
(27, 'Opération de débit', '2018-08-03 17:52:10', 100000.00, 'DB', '002312329');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `Agence_Client` FOREIGN KEY (`numag`) REFERENCES `agence` (`numag`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Contraintes pour la table `compte`
--
ALTER TABLE `compte`
  ADD CONSTRAINT `FR_Client_Compte` FOREIGN KEY (`numcli`) REFERENCES `client` (`numcli`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
