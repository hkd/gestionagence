/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import image.fond.ChargeurImage;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Event;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.border.TitledBorder;
import metier.Service;

/**
 *
 * @author Hamadou DAO
 */
public class Start extends JFrame implements WindowListener {

    AcceuilAgenceUser acceuilAgenceUser;
    public static int i = 1;
    public static Service accesBD = new Service();

    public Start() throws HeadlessException {
        super(VariableGlobale.nomLogiciel);
        setSize(1024, 640);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setMinimumSize(getSize());
        setIconImage(new ChargeurImage(VariableGlobale.cheminIcon).getImage());
        addWindowListener(this);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        JMenuBar j = new BarDeMenu(this);
        setJMenuBar(j);
        setLocationRelativeTo(null);
        getContentPane().add(fond, BorderLayout.NORTH);
        fond.setVisible(false);
        JButton next = new JButton(new ChargeurImage(VariableGlobale.cheminSuiv));
        JButton prec = new JButton(new ChargeurImage(VariableGlobale.cheminPrec));
        JLabel lefond=new JLabel();
        
        fond.add(prec);
        fond.add(lefond);
        fond.add(next);
        next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VariableGlobale.cheminFondEcran
                        = VariableGlobale.cheminFondEcran.substring(0, VariableGlobale.cheminFondEcran.lastIndexOf("/") + 1) + i + ".png";
                i++;
                if (i == 16) {
                    i = 1;
                }
                lefond.setText(""+i);
                VariableGlobale.saveFondEcran();
                contenaire.repaint();
            }
        });
        prec.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                VariableGlobale.cheminFondEcran
                        = VariableGlobale.cheminFondEcran.substring(0, VariableGlobale.cheminFondEcran.lastIndexOf("/") + 1) + i + ".png";
                i--;
                if (i == 0) {
                    i = 13;
                }
                lefond.setText(""+i);
                VariableGlobale.saveFondEcran();
                contenaire.repaint();
            }
        });
        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 3) {
                    fond.setVisible(!fond.isVisible());
                }
            }

        });
        getContentPane().add(contenaire);
        acceuilAgenceUser = new AcceuilAgenceUser(this);
        ChangeScene(acceuilAgenceUser);
        setVisible(true);
        accesBD.seConnecter();

    }
    public JPanel fond = new JPanel(new FlowLayout(FlowLayout.CENTER));
    public JPanel contenaire = new JPanel(new CardLayout()) {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
            g.drawImage(new ChargeurImage(VariableGlobale.cheminFondEcran).getImage(), 0, 0, this.getWidth(), this.getHeight(), null);
        }

    };

    @Override
    public boolean keyDown(Event evt, int key) {
        System.out.println("vue.Start.keyDown()");
        return super.keyDown(evt, key); //To change body of generated methods, choose Tools | Templates.
    }

    public void selectAccueil() {
        acceuilAgenceUser.setVisible(true);
    }

    /**
     *
     * @param pan
     * @since cette methode permet de changer un panneau
     */
    public void ChangeScene(JPanel pan) {
        pan.setOpaque(false);
        contenaire.setBorder(new TitledBorder(" "));
        contenaire.add(pan);
    }

    public void quitter() {
        if (new msg_confir_quitter().getReturnStatus() == 1) {
            System.exit(0);
        }
    }

    public static void main(String[] args) {
        for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {

            if ("Nimbus".equals(info.getName())) {
                try {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InstantiationException ex) {
                    Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IllegalAccessException ex) {
                    Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
                } catch (UnsupportedLookAndFeelException ex) {
                    Logger.getLogger(Start.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;
            }
        }
        VariableGlobale.initFondEcran();
        new Start();

    }
/**
 * 
 * @since : les ecouteurs d'evenement 
 */
    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {
        quitter();
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {
    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
