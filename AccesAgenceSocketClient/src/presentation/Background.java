/**
 *
 * @author Hamadou DAO
 */
package presentation;

import image.fond.ChargeurImage;
import java.awt.Graphics;
import javax.swing.JPanel;

/**
 * @param Ce panneau permet de mettre le fond d'arrière plan Il sera hérité par
 * tous les panneaux
 */
public class Background extends JPanel {

    public Background() {
        
    }

    
    @Override
    public void paintComponent(Graphics g) {
        super.paintComponents(g);
        g.drawImage(new ChargeurImage(VariableGlobale.cheminFondEcran).getImage(), 0, 0,this.getWidth(),this.getHeight(), null);
    }

}
