/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import entities.Client;
import entities.Compte;
import entities.Operation;
import image.fond.ChargeurImage;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.util.ArrayList;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;
import metier.AutoComplete;
import metier.AutoCompleteModel;

/**
 *
 * @author Hamadou DAO
 */
public class ReleverDeCompte extends javax.swing.JDialog {

    /**
     * Creates new form Relever compte
     */
    AutoCompleteModel model = new AutoCompleteModel();
    AutoComplete panel;

    public ReleverDeCompte(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        panel = new AutoComplete();
        initComponents();
        initListeClient();
        init();
        panel.setModel(model);
        setLocationRelativeTo(null);
        setResizable(false);
        panel.setSize(200, 40);
        panel.setMinimumSize(panel.getSize());
        panel.setPreferredSize(panel.getSize());
        jPanel3.add(panel);
        jScrollPane1.setVisible(false);
        jComboBox4.setVisible(false);
        panel.setVisible(true);
        panel.zoneTexte.addFocusListener(focusAdapter);
        panel.zoneTexte.addActionListener(actionListener);
        pack();
        setVisible(true);
    }

    // ce qu'il doit faire en zonetexte
    private void actionAfaireZoneTexte() {

        Compte c;
        try {
            c = (Compte) panel.object();
        } catch (ClassCastException e) {
            libcpt.setText("");
            sens.setText("");
            solde.setText("");
            removeAllTableData();
            return;
        } catch (Exception e) {
            return;
        }
        libcpt.setText(c.getLibcpt());
        sens.setText(c.getSens());
        solde.setText(c.getSolde());
        if (sens.getText().contains("CR")) {
            jPanel2.setBackground(Color.GREEN);
        }
        if (sens.getText().contains("DB")) {
            jPanel2.setBackground(Color.RED);
        }
        if (sens.getText().compareTo("") == 0) {
            jPanel2.setBackground(null);
        }

        remplirTable(c);
        etatValider = false;
        jComboBox3.setSelectedItem(c.getClient().toString());
    }

    FocusAdapter focusAdapter = new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
            actionAfaireZoneTexte();
            panel.fenetreRecherche.setVisible(false);
        }

    };
    ActionListener actionListener = new ActionListener() {
        public void actionPerformed(java.awt.event.ActionEvent evt) {
            actionAfaireZoneTexte();
        }
    };

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel(){
            @Override
            public void paintComponent(Graphics g) {
                super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
                g.drawImage(new ChargeurImage(VariableGlobale.cheminFondEcran).getImage(), 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };
        jPanel5 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jComboBox4 = new javax.swing.JComboBox<>();
        jComboBox3 = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        libcpt = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        solde = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        sens = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Relévé Compte");

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(""), "Relever de compte", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.BOTTOM));
        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.BorderLayout());

        jPanel5.setMinimumSize(new java.awt.Dimension(452, 170));
        jPanel5.setOpaque(false);

        jPanel3.setMinimumSize(new java.awt.Dimension(200, 20));
        jPanel3.setLayout(new java.awt.CardLayout());

        jComboBox4.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox4ActionPerformed(evt);
            }
        });
        jPanel3.add(jComboBox4, "card2");

        jComboBox3.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jComboBox3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jComboBox3ActionPerformed(evt);
            }
        });

        jPanel2.setLayout(new java.awt.GridLayout(0, 2));

        jLabel1.setFont(new java.awt.Font("Garamond", 1, 14)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Libelle :");
        jPanel2.add(jLabel1);

        libcpt.setFont(new java.awt.Font("Garamond", 0, 14)); // NOI18N
        libcpt.setText("jLabel3");
        jPanel2.add(libcpt);

        jLabel2.setFont(new java.awt.Font("Garamond", 1, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Solde :");
        jPanel2.add(jLabel2);

        solde.setFont(new java.awt.Font("Garamond", 0, 14)); // NOI18N
        solde.setText("jLabel4");
        jPanel2.add(solde);

        jLabel3.setFont(new java.awt.Font("Garamond", 1, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Sens :");
        jPanel2.add(jLabel3);

        sens.setFont(new java.awt.Font("Garamond", 0, 14)); // NOI18N
        sens.setText("jLabel4");
        jPanel2.add(sens);

        jButton1.setBackground(new java.awt.Color(0, 204, 102));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/eye.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jComboBox3, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, 356, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(261, 261, 261)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel5Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addContainerGap()))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jComboBox3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 107, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                    .addContainerGap(42, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(40, 40, 40)))
        );

        jPanel1.add(jPanel5, java.awt.BorderLayout.NORTH);

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(jTable1);

        jPanel1.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        jScrollPane1.setVisible(!jScrollPane1.isVisible());
        pack();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jComboBox3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox3ActionPerformed
        // TODO add your handling code here:
        if (!etatValider) {
            etatValider = true;
            return;
        }
        if (sens.getText().compareTo("") == 0) {
            jPanel2.setBackground(null);
        }
        if (jComboBox3.getSelectedIndex() == 0) {
            libcpt.setText("");
            sens.setText("");
            solde.setText("");
            initListeClient();
            model.addAll(Start.accesBD.listeComptes().toArray());
            panel.setVisible(true);
            jComboBox4.setVisible(false);

        } else if (jComboBox3.getSelectedIndex() > 0) {
            initListeClientFiltreCompte();
            panel.setVisible(false);
            jComboBox4.setVisible(true);
        }
    }//GEN-LAST:event_jComboBox3ActionPerformed

    private void jComboBox4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jComboBox4ActionPerformed
        // TODO add your handling code here:
        if (jComboBox4.getSelectedItem() == null) {
            libcpt.setText("");
            sens.setText("");
            solde.setText("");
            return;
        }
        Compte c = (Compte) jComboBox4.getSelectedItem();
        libcpt.setText(c.getLibcpt());
        sens.setText(c.getSens());
        solde.setText(c.getSolde());
        if (sens.getText().contains("CR")) {
            jPanel2.setBackground(Color.GREEN);
        }
        if (sens.getText().contains("DB")) {
            jPanel2.setBackground(Color.RED);
        }
        if (sens.getText().compareTo("") == 0) {
            jPanel2.setBackground(null);
        }
        remplirTable(c);
        etatValider = false;
        jComboBox3.setSelectedItem(c.getClient().toString());
    }//GEN-LAST:event_jComboBox4ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ReleverDeCompte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ReleverDeCompte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ReleverDeCompte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ReleverDeCompte.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                ReleverDeCompte dialog = new ReleverDeCompte(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jComboBox3;
    private javax.swing.JComboBox<String> jComboBox4;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel libcpt;
    private javax.swing.JLabel sens;
    private javax.swing.JLabel solde;
    // End of variables declaration//GEN-END:variables
    int i = 0;
    public DefaultTableModel modele;

    private boolean etatValider = false;

    public void initListeClient() {
        jComboBox3.removeAllItems();
        jComboBox3.addItem("");
        for (Client a : Start.accesBD.listeClients()) {
            jComboBox3.addItem(a.toString());
        }
        /*
        jComboBox4.setModel(new DefaultComboBoxModel(Start.accesBD.listeComptes().toArray()));
        Compte c = (Compte) jComboBox4.getSelectedItem();
        if (c == null) {
            jPanel2.setBackground(null);
            return;
        }
        if (c.getSens().contains("CR")) {
            jPanel2.setBackground(Color.GREEN);
        }
        if (c.getSens().contains("DB")) {
            jPanel2.setBackground(Color.RED);
        }
        sens.setText(c.getSens());
        solde.setText(c.getSolde());
        libcpt.setText(c.getLibcpt());*/
    }

    public void initListeClientFiltreCompte() {
        jComboBox4.setModel(new DefaultComboBoxModel(Start.accesBD.listeComptes(Start.accesBD.listeClients().get(jComboBox3.getSelectedIndex() - 1)).toArray()));
        Compte c = (Compte) jComboBox4.getSelectedItem();
        if (c == null) {
            jPanel2.setBackground(null);
            return;
        }
        if (c.getSens().contains("CR")) {
            jPanel2.setBackground(Color.GREEN);
        }
        if (c.getSens().contains("DB")) {
            jPanel2.setBackground(Color.RED);
        }
        libcpt.setText(c.getLibcpt());
        sens.setText(c.getSens());
        solde.setText(c.getSolde());
//        for (Compte a : Start.accesBD.listeComptes(Start.accesBD.listeClients().get(jComboBox3.getSelectedIndex() - 1))) {
//            jComboBox4.addItem(a.getLibcpt());
        //       }

    }

    private void removeAllTableData() {
        for (int i = 0; i < modele.getRowCount(); i++) {
            modele.removeRow(0);
        }
    }

    private void init() {
        jScrollPane1.getViewport().setOpaque(false);
        modele = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; //To change body of generated methods, choose Tools | Templates.
            }

        };

        modele.addColumn("#");
        modele.addColumn("Date");
        modele.addColumn("Libelle");
        modele.addColumn("Solde Initial");
        modele.addColumn("Solde");
        modele.addColumn("Solde Final");
        modele.addColumn("Sens");
        jTable1.setModel(modele);
    }

    private void remplirTable(Compte compte) {
        int ligne = 0;
        float montant = 0;
        removeAllTableData();
        for (Operation o : Start.accesBD.listeOperations(compte)) {
            int i = 0;  // permet de parcourir les colonnes du tableau, 
            //apres avoir mis une valeur dans une cellule passe a l'autre

            
            modele.addRow(new Object[0]);
            modele.setValueAt("" + o.getNumop(), ligne, i++);
            modele.setValueAt(o.getDateop(), ligne, i++);
            modele.setValueAt(o.getLibop(), ligne, i++);
            modele.setValueAt(montant, ligne, i++);
            modele.setValueAt(o.getMontop(), ligne, i++);
            
            String sens;
            if (o.getSensop().compareTo("CR") == 0) {
                sens = "Versement";
                montant+=Float.parseFloat(o.getMontop());
            } else {
                sens = "retrait";
                montant-=Float.parseFloat(o.getMontop());
            }
            modele.setValueAt(montant, ligne, i++);
            modele.setValueAt(sens, ligne, i++);
            ligne++;
        }
    }

}
