/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import entities.Client;
import entities.Compte;
import entities.Operation;
import image.fond.ChargeurImage;
import java.awt.Graphics;
import java.util.ArrayList;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Hamadou DAO
 */
public class GestionOperation extends javax.swing.JDialog {

    /**
     * Creates new form GestionCompte
     */
    Client client;
    Compte compte;

    public GestionOperation(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        init();
        remplirTable();
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public GestionOperation(java.awt.Frame parent, boolean modal, Client client) {
        super(parent, modal);
        compte = null;
        this.client = client;
        initComponents();
        jLabel1.setText("La liste des operations (" + client + ")");
        init();
        remplirTable();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    public GestionOperation(java.awt.Frame parent, boolean modal, Compte compte) {
        super(parent, modal);
        client = null;
        this.compte = compte;
        initComponents();
        jLabel1.setText("La liste des operations du compte(" + compte.getNumcpt() + ")");
        init();
        remplirTable();
        pack();
        setResizable(false);
        setLocationRelativeTo(null);
        setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
                g.drawImage(new ChargeurImage(VariableGlobale.cheminFondEcran).getImage(), 0, 0, this.getWidth(), this.getHeight(), null);
            }
        };
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
                g.drawImage(new ChargeurImage(VariableGlobale.cheminAjouter).getImage(), 5, 5,this.getWidth()-10,this.getHeight()-10, null);
            }

        };
        jButton3 = new javax.swing.JButton(){
            @Override
            protected void paintComponent(Graphics g) {
                super.paintComponent(g); //To change body of generated methods, choose Tools | Templates.
                g.drawImage(new ChargeurImage(VariableGlobale.cheminSupprimer).getImage(), 5, 5,this.getWidth()-10,this.getHeight()-10, null);
            }

        };
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Gestion des Opérations");

        jPanel4.setPreferredSize(new java.awt.Dimension(840, 453));
        jPanel4.setLayout(new java.awt.BorderLayout());

        jPanel2.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Garamond", 1, 36)); // NOI18N
        jLabel1.setText("Gestion des Opérations");
        jPanel2.add(jLabel1);

        jPanel4.add(jPanel2, java.awt.BorderLayout.NORTH);

        jPanel3.setOpaque(false);
        jPanel3.setPreferredSize(new java.awt.Dimension(100, 300));
        jPanel3.setLayout(new javax.swing.BoxLayout(jPanel3, javax.swing.BoxLayout.Y_AXIS));

        jButton1.setText("jButton1");
        jButton1.setToolTipText("Ajouter");
        jButton1.setMaximumSize(new java.awt.Dimension(120, 80));
        jButton1.setMinimumSize(new java.awt.Dimension(120, 80));
        jButton1.setPreferredSize(new java.awt.Dimension(120, 80));
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton1);

        jButton3.setText("jButton1");
        jButton3.setToolTipText("Supprimer");
        jButton3.setMaximumSize(new java.awt.Dimension(120, 80));
        jButton3.setMinimumSize(new java.awt.Dimension(120, 80));
        jButton3.setPreferredSize(new java.awt.Dimension(120, 80));
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });
        jPanel3.add(jButton3);

        jPanel4.add(jPanel3, java.awt.BorderLayout.EAST);

        jPanel1.setOpaque(false);
        jPanel1.setLayout(new java.awt.BorderLayout());

        jScrollPane1.setOpaque(false);

        jTable1.setAutoCreateRowSorter(true);
        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "id", "Agence", "Nom", "Adresse"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTable1.setColumnSelectionAllowed(true);
        jTable1.setDragEnabled(true);
        jTable1.setOpaque(false);
        jScrollPane1.setViewportView(jTable1);

        jPanel1.add(jScrollPane1, java.awt.BorderLayout.CENTER);

        jPanel4.add(jPanel1, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanel4, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        if (compte != null) {
            new AjouterOperation(null, true, compte);
        } else if (client != null) {
            new AjouterOperation(null, true, client);
        } else {
            new AjouterOperation(null, true);
        }
        remplirTable();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
        if (jTable1.getSelectedRow() > -1 && new msg_confir_supprimer().getReturnStatus()
                == msg_confir_supprimer.oui) {
            new msg_suppr_info(Start.accesBD.supprimerOperation(listeOperations
                    .get(jTable1.getSelectedRow())));
            remplirTable();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GestionOperation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GestionOperation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GestionOperation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GestionOperation.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                GestionOperation dialog = new GestionOperation(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTable1;
    // End of variables declaration//GEN-END:variables
public DefaultTableModel modele;

    private void init() {
        jScrollPane1.getViewport().setOpaque(false);
        modele = new DefaultTableModel() {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false; //To change body of generated methods, choose Tools | Templates.
            }

        };

        modele.addColumn("#");
        modele.addColumn("Date");
        if (compte == null) {
            modele.addColumn("Numéro Compte");
        }

        modele.addColumn("Libelle");
        modele.addColumn("Solde");
        modele.addColumn("Sens");
        if (client == null && compte == null) {
            modele.addColumn("Client");
        }
        jTable1.setModel(modele);
    }
    ArrayList<Operation> listeOperations;

    private void remplirTable() {
        int ligne = 0;
        removeAllTableData();
        listeOperations = new ArrayList<>();
        if (compte != null) {
            listeOperations = Start.accesBD.listeOperations(compte);
        } else if (client != null) {
            listeOperations = Start.accesBD.listeOperations(client);
        } else {
            listeOperations = Start.accesBD.listeOperations();
        }

        for (Operation o : listeOperations) {
            int i = 0;  // permet de parcourir les colonnes du tableau, 
            //apres avoir mis une valeur dans une cellule passe a l'autre
            modele.addRow(new Object[0]);
            modele.setValueAt("" + o.getNumop(), ligne, i++);
            modele.setValueAt(o.getDateop(), ligne, i++);
            if (compte == null) {
                modele.setValueAt(o.getCompte().getLibcpt(), ligne, i++);
            }
            modele.setValueAt(o.getLibop(), ligne, i++);
            modele.setValueAt(o.getMontop(), ligne, i++);
            modele.setValueAt(o.getSensop(), ligne, i++);
            if (client == null && compte == null) {
                modele.setValueAt(o.getCompte().getClient(), ligne, i++);
            }
            ligne++;
        }
    }

    private void removeAllTableData() {
        for (int i = 0; i < modele.getRowCount(); i++) {
            modele.removeRow(0);
        }
    }

}
