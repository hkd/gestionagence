/**
 *
 * @author Hamadou DAO
 */
package presentation;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import static presentation.Start.i;

/**
 * @since ici ce touvre la barre de menu
 */
public class BarDeMenu extends JMenuBar implements ActionListener,KeyListener {
    
    JMenu fichier = new JMenu("Fichier");
    JMenuItem apropos = new JMenuItem("A propos");
    JMenuItem changer = new JMenuItem("Charger de photo");
    JMenuItem quitter = new JMenuItem("Quitter");
    JMenu parametre = new JMenu("Paramètre");
    JMenu clients=new JMenu("Gestion des clients");
    JMenuItem ajouterClient=new JMenuItem("Ajouter un client");
    JMenuItem listerClient=new JMenuItem("La liste des clients");
    JMenu comptes=new JMenu("Gestion des comptes");
    JMenuItem ajouterCompte=new JMenuItem("Ajouter un compte");
    JMenuItem listerCompte=new JMenuItem("La liste des comptes");
    JMenu agences=new JMenu("Gestion des agences");
    JMenuItem ajouterAgence=new JMenuItem("Ajouter Agence");
    JMenuItem listerAgence=new JMenuItem("La liste des agences");
    JMenuItem grilleAgence=new JMenuItem("Grille des agences");
    JMenu operations=new JMenu("Gestion des operations");
    JMenuItem ajouterOperation=new JMenuItem("Faire opération");
    JMenuItem listerOperation=new JMenuItem("Lister opération");
    JFrame parent;
    
    public BarDeMenu(JFrame parent) {
        super();
        changer.setActionCommand("changer");
        quitter.setActionCommand("quitter");
        fichier.add(changer);
        fichier.add(quitter);
        clients.add(ajouterClient);
        clients.add(listerClient);
        comptes.add(ajouterCompte);
        comptes.add(listerCompte);
        agences.add(ajouterAgence);
        agences.add(listerAgence);
        agences.add(grilleAgence);
        operations.add(ajouterOperation);
        operations.add(listerOperation);
        parametre.add(agences);
        parametre.add(clients);
        parametre.add(comptes);
        parametre.add(operations);
        add(fichier);
        add(parametre);
        add(apropos);
        
        quitter.addKeyListener(this);
        apropos.addActionListener(this);
        changer.addActionListener(this);
        quitter.addActionListener(this);
        grilleAgence.addActionListener(this);
        ajouterAgence.addActionListener(this);
        listerAgence.addActionListener(this);
        listerClient.addActionListener(this);
        ajouterClient.addActionListener(this);
        ajouterOperation.addActionListener(this);
        listerOperation.addActionListener(this);
        this.parent = parent;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().compareToIgnoreCase(changer.getActionCommand()) == 0) {
            new ChangerPhoto();
            return;
        }
        if (e.getActionCommand().compareToIgnoreCase(quitter.getActionCommand()) == 0) {
            ((Start) parent).quitter();
            return;
        }
        if(e.getActionCommand().compareToIgnoreCase(apropos.getActionCommand())==0){
            new Apropo();
            return;
        }
        if(Start.accesBD.verif())return;
        if(e.getActionCommand().compareToIgnoreCase(ajouterAgence.getActionCommand())==0){
            new AjouterAgence(parent, true);
            return;
        }
        if(e.getActionCommand().compareToIgnoreCase(listerAgence.getActionCommand())==0){
            new GestionAgence(parent, true);
            return;
        }
        if(e.getActionCommand().compareToIgnoreCase(ajouterClient.getActionCommand())==0){
            new AjouterClient(parent, true);
            return;
        }
        if(e.getActionCommand().compareToIgnoreCase(listerClient.getActionCommand())==0){
            new GestionClient(parent, true);
            return;
        }
        if(e.getActionCommand().compareToIgnoreCase(grilleAgence.getActionCommand())==0){
            new GestionAgence(parent, true,0);
            return;
        }
        if(e.getActionCommand().compareToIgnoreCase(ajouterCompte.getActionCommand())==0){
            new AjouterCompte(parent, true);
            return;
        }
        if(e.getActionCommand().compareToIgnoreCase(listerCompte.getActionCommand())==0){
            new GestionCompte(parent, true);
            return;
        }
        if(e.getActionCommand().compareToIgnoreCase(ajouterOperation.getActionCommand())==0){
            new AjouterOperation(parent, true);
            return;
        }
        if(e.getActionCommand().compareToIgnoreCase(listerOperation.getActionCommand())==0){
            new GestionOperation(parent, true);
            return;
        }
        System.err.println(e.getActionCommand());
    }

   @Override
    public void keyTyped(KeyEvent e) {
        //     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("vue.Start.keyTyped()");
    }

    @Override
    public void keyPressed(KeyEvent e) {
        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        if (e.getKeyCode() == KeyEvent.VK_LEFT && e.getModifiers() == KeyEvent.CTRL_MASK) {
            VariableGlobale.cheminFondEcran = 
                    VariableGlobale.cheminFondEcran.substring
        (0, VariableGlobale.cheminFondEcran.lastIndexOf("/"))+i+".png";
            i--;if(i==0)i=13;
            ((Start)parent).contenaire.repaint();
        }
        if (e.getKeyCode() == KeyEvent.VK_RIGHT && e.getModifiers() == KeyEvent.CTRL_MASK) {
            
        }
        System.out.println(e.getKeyCode());
        
    }

    @Override
    public void keyReleased(KeyEvent e) {
        //    throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        System.out.println("vue.Start.keyReleased()");
    }
    
}
