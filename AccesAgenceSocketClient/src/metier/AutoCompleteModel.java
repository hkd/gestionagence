package metier;

import java.util.ArrayList;
import java.util.List;

/**
 * <b>Cette classe definit les donnees necessaires � l'autocomplete</b>
 * <p>
 * Cette classe est composee de la liste des mots qui seront recherhes
 * </p>
 *
 * @author Julien
 * @version 1.0
 *
 */
public class AutoCompleteModel {

    /**
     * La liste dans laquelle on recherche
     */
    private List<String> mots;
    public Object[] motsObject;
    /**
     * Constructeur du modele
     */
    public AutoCompleteModel() {
        mots = new ArrayList<String>();
    }

    /**
     * Fonction qui permet d'ajouter un mot a la liste des recherches
     *
     * @param mot Le mot a ajouter
     */
    public void add(String mot) {
        mots.add(mot);
    }

    /**
     * Fonction qui permet d'ajouter une liste de mots � la liste des recherches
     *
     * @param mots La liste des mots � ajouter
     */
    public void addAll(List<String> mots) {
        this.mots.addAll(mots);
    }

    public void addAll(Object[] mots) {
        this.mots=new ArrayList<String>();
        motsObject=mots;
        for (Object mot : mots) {
            this.mots.add(mot.toString());
        }
    }


    /**
     * Fonction qui permet de retourner la liste de chauines correspondant � un
     * debut de mot
     *
     * @param debut Le debut de mot
     * @return La liste des chaines correspondantes
     */
    public List<String> getChainesCorrespondates(String debut) {
        List<String> res = new ArrayList<String>();
        /*for (String s : mots) {
            if (debut != null && s.length() >= debut.length() && s.substring(0, debut.length()).equals(debut)) {
                res.add(s);
            }
        }*/
        for (String s : mots) {
            if (debut != null && s.length() >= debut.length() && s.toLowerCase().trim().contains(debut.trim().toLowerCase())) {
                res.add(s);
            }
        }
        return res;
    }

}
