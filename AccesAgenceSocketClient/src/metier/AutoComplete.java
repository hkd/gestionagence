package metier;

import java.awt.Dialog;
import java.awt.GridLayout;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JWindow;

/**
 * <b>Cette classe definit le composant Swing AutoComplete</b>
 * <p>
 * Cette classe est caracterisee par les informations suivantes :
 * <ul>
 * <li>Un AutocompleteModel</li>
 * </ul>
 * </p>
 * <p>
 * Ce composant permet d'effectuer une recheche et de visualiser directement le
 * resultat
 * </p>
 *
 * @author Hamadou DAO
 * @verion 1.0
 *
 */
public class AutoComplete extends JPanel {

    private static final long serialVersionUID = 1L;

    /**
     * La zone de texte dans laquelle on fait la recherche
     */
    public JTextField zoneTexte;

    /**
     * La Jwindow dans laquelle s'affichent les resultats de la recherche
     */
    public JWindow fenetreRecherche;

    /**
     * La liste des resultats de la recherche
     */
    private JList resultats;

    /**
     * Le modele de la liste des resultats
     */
    private DefaultListModel modelListe;

    /**
     * Le modele de l'autocomplete
     */
    private AutoCompleteModel model;

    /**
     * Constructeur de l'autocomplete
     *
     * @param model Le modele de l'autocomplete
     */
    public AutoComplete() {
        model = new AutoCompleteModel();
        init();
    }

    public AutoComplete(AutoCompleteModel model) {
        this.model = model;
        init();
    }

    private void init() {
        zoneTexte = new JTextField();
        modelListe = new DefaultListModel();
        resultats = new JList(modelListe);
        resultats.setBorder(BorderFactory.createEtchedBorder());
        fenetreRecherche = new JWindow();
        fenetreRecherche.setModalExclusionType(Dialog.ModalExclusionType.APPLICATION_EXCLUDE);
        fenetreRecherche.add(new JScrollPane(resultats));
        this.addFocusListener(focusAdapter);
        zoneTexte.addKeyListener(keyListener);
        setLayout(new GridLayout(1, 0));
        add(zoneTexte);
    }

    public void setModel(AutoCompleteModel model) {
        this.model = model;
    }

    FocusAdapter focusAdapter = new FocusAdapter() {
        @Override
        public void focusLost(FocusEvent e) {
            fenetreRecherche.setVisible(false);
        }

    };
    KeyListener keyListener = new KeyListener() {

        @Override
        public void keyTyped(KeyEvent e) {
        }

        @Override
        public void keyReleased(KeyEvent e) {
            if (zoneTexte.getText() == null || zoneTexte.getText().length() == 0) {
                fenetreRecherche.setVisible(false);
            }
            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                if (resultats.getSelectedIndex() < resultats.getModel().getSize()) {
                    resultats.setSelectedIndex(resultats.getSelectedIndex() + 1);
                } else {
                    resultats.setSelectedIndex(0);
                }
            } else if (e.getKeyCode() == KeyEvent.VK_UP) {
                if (resultats.getSelectedIndex() != resultats.getModel().getSize()) {
                    resultats.setSelectedIndex(resultats.getSelectedIndex() - 1);
                } else {
                    resultats.setSelectedIndex(resultats.getModel().getSize());
                }
            } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                zoneTexte.setText(resultats.getSelectedValue().toString());
                fenetreRecherche.setVisible(false);
            } else {
                update();
            }
        }

        @Override
        public void keyPressed(KeyEvent e) {
        }

    };

    public Object object() {
        for (Object o : model.motsObject) {
            if (o.toString().compareTo(zoneTexte.getText()) == 0) {
                return o;
            }
        }
        return new Object();
    }

    /**
     * Fonction qui permet de mettre a jour les resultats de la recherche
     */
    public void update() {
        List<String> correspondants = model.getChainesCorrespondates(zoneTexte.getText());
        modelListe.clear();
        if (correspondants.isEmpty()) {
            fenetreRecherche.setVisible(false);
            return;
        } else {
            for (String s : correspondants) {
                modelListe.addElement(s);
            }
        }
        fenetreRecherche.setBounds((int) getLocationOnScreen().getX(), (int) getLocationOnScreen().getY() + zoneTexte.getHeight(), getWidth(), 3 * zoneTexte.getHeight());
        fenetreRecherche.setVisible(true);
        resultats.setSelectedIndex(0);
    }

}
