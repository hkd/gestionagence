/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

import entities.Agence;
import entities.Client;
import entities.Compte;
import entities.Operation;
import image.fond.ChargeurImage;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import presentation.VariableGlobale;
import presentation.msg_Information;

/**
 *
 * @author Hamadou DAO
 */
public class Service extends Thread {

    /**
     * Declaration des variables. Les flux entres-sortis, socket
     * Une variable pour recuperer l'adresse et une variable pour avoir l'etat de la connexion
     */
    private Socket socket;
    ObjectInputStream ois;
    ObjectOutputStream oos;
    DataOutputStream out;
    DataInputStream in;
    BufferedOutputStream boos;
    BufferedInputStream bois;
    boolean reponse_etat;
    private String adresse;
    /**
     * Nous avons le constructeur
     */
    public Service() {
    }

    /**
     * La methode qui permet de se connecter au serveur 
     */
    @SuppressWarnings("empty-statement")
    public void seConnecter() {
        File f = new File("data/adresse");
        adresse = "localhost";

        try (BufferedReader lire = new BufferedReader(new FileReader(f))) {
            adresse = lire.readLine();
            lire.close();
        } catch (FileNotFoundException ex) {
           // Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            System.err.println(ex.getMessage());
        } catch (IOException ex) {
            System.err.println(ex.getMessage());           
            // Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            socket = new Socket(adresse, 8000);
            out = new DataOutputStream(socket.getOutputStream());
            in = new DataInputStream(socket.getInputStream());
            oos = new ObjectOutputStream(socket.getOutputStream());;
            ois = new ObjectInputStream(socket.getInputStream());
        } catch (IOException ex) {
            new msg_Information("Impossible de se connecter");
        }
    }

    /**
     * La methode de verification de la connexion
     * @return 
     */
    public boolean verif() {
        if (!isConnecter()) {
            new msg_Information("Veuillez-vous connecter");
            return true;
        }
        return false;
    }
    /*
    * Methode pour recuperer l'adresse du serveur
    */
    public String getAdresse() {
        return adresse;
    }

    public boolean isConnecter() {
        if (socket != null) {
            return socket.isConnected();
        } else {
            return false;
        }
    }
    /**
     * Gestion Agence
     * Fonction : ajouter,modifier,supprimer,lister
     */
    
    /**
     * 
     * @param agence
     * @return 
     */
    public boolean ajouterAgence(Agence a) {
        try {
            oos.writeObject("ajouter agence");
            oos.flush();
            oos.writeObject(a);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean modifierAgence(Agence a) {
        try {
            oos.writeObject("modifier agence");
            oos.flush();
            oos.writeObject(a);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean supprimerAgence(Agence a) {
        try {
            oos.writeObject("supprimer agence");
            oos.flush();
            oos.writeObject(a);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public ArrayList<Agence> listeAgences() {
        try {
            oos.writeObject("lister agence");
            oos.flush();
            return (ArrayList<Agence>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public int nombreClient(Agence agence) {
        try {
            oos.writeObject("nombre de client par agence");
            oos.flush();
            oos.writeObject(agence);
            oos.flush();
            return in.readInt();
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return 0;
    }
    
    /**
     * Gestion client
     * @Fonction : ajouter,modifier,supprimer,lister
     * @Fonction : lister client d'une agence
     */
    /**
     * 
     * @param client
     * @return 
     */
    public boolean ajouterClient(Client c) {
        try {
            oos.writeObject("ajouter client");
            oos.flush();
            oos.writeObject(c);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean modifierClient(Client c) {
        try {
            oos.writeObject("modifier client");
            oos.flush();
            oos.writeObject(c);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean supprimerClient(Client c) {
        try {
            oos.writeObject("supprimer client");
            oos.flush();
            oos.writeObject(c);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public ArrayList<Client> listeClients() {
        try {
            oos.writeObject("la liste des clients");
            oos.flush();
            ArrayList<Client> liste = (ArrayList<Client>) ois.readObject();
            return liste;
        } catch (IOException | ClassNotFoundException ex) {
            new msg_Information("" + ex.getMessage());
        }
        return new ArrayList<>();
    }

    public ArrayList<Client> listeClients(Agence agence) {
        try {
            oos.writeObject("la liste des clients d'une agence");
            oos.flush();
            oos.writeObject(agence);
            oos.flush();
            return (ArrayList<Client>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public Client findClient(int id) {
        return null;
    }
    /**
     * Gestion Compte
     * @Fonction : ajouter,modifier,supprimer,lister
     * @Fonction :lister comptes d'un client
     */
    /**
     * 
     * @param compte
     * @return 
     */
    public boolean ajouterCompte(Compte c) {
        try {
            oos.writeObject("ajouter compte");
            oos.flush();
            oos.writeObject(c);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean modifierCompte(Compte c) {
        try {
            oos.writeObject("modifier compte");
            oos.flush();
            oos.writeObject(c);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean supprimerCompte(Compte c) {
        try {
            oos.writeObject("supprimer compte");
            oos.flush();
            oos.writeObject(c);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public ArrayList<Compte> listeComptes() {
        try {
            oos.writeObject("la liste des comptes");
            oos.flush();
            return (ArrayList<Compte>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Compte> listeComptes(Client client) {
        try {
            oos.writeObject("la liste des comptes d'un client");
            oos.flush();
            oos.writeObject(client);
            oos.flush();
            return (ArrayList<Compte>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    /**
     * Gestion operation
     * @Fonction : ajouter,supprimer, lister
     * @Fonction : lister operations d'un compte,lister operations des comptes d'un client
     */
    /**
     * 
     * @param operation
     * @return 
     */
    public boolean ajouterOperation(Operation o) {
        try {
            oos.writeObject("ajouter operation");
            oos.flush();
            oos.writeObject(o);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean modifierOperation(Operation o) {
        try {
            oos.writeObject("modifier operation");
            oos.flush();
            oos.writeObject(o);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public boolean supprimerOperation(Operation o) {
        try {
            oos.writeObject("supprimer operation");
            oos.flush();
            oos.writeObject(o);
            oos.flush();
            return (in.readBoolean());
        } catch (IOException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }

    public ArrayList<Operation> listeOperations() {
        try {
            oos.writeObject("la liste des operations");
            oos.flush();
            return (ArrayList<Operation>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Operation> listeOperations(Compte c) {
        try {
            oos.writeObject("la liste des operations d'un compte client");
            oos.flush();
            oos.writeObject(c);
            oos.flush();
            return (ArrayList<Operation>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ArrayList<Operation> listeOperations(Client client) {
        try {
            oos.writeObject("la liste des operations d'un client");
            oos.flush();
            oos.writeObject(client);
            oos.flush();
            return (ArrayList<Operation>) ois.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(Service.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public ImageIcon recupererPhoto() {
        if (!isConnecter()) {
            return new ChargeurImage(VariableGlobale.cheminPhoto);
        }
        return new ImageIcon(VariableGlobale.imageClient);
    }
}
