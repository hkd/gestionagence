/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package metier;

/**
 * Classe de gestion des erreurs de chargement des fichiers
 */
/**
 *
 * @author Hamadou DAO
 */
public class ErrFichier extends Exception {

    public ErrFichier(String message) {
        super(message);
    }

}
